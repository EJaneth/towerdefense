﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : MonoBehaviour
{
   [SerializeField] public Canvas SeleccionNiveles;
    [SerializeField] public Canvas ReturnMain;

	// Use this for initialization
	void Start ()
    {
        SeleccionNiveles.enabled = false;
        ReturnMain.enabled = true;

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SelectLevel()
    {
        SeleccionNiveles.enabled = true;
        ReturnMain.enabled = false;
    }

    public void Return()
    {
        SeleccionNiveles.enabled = false;
        ReturnMain.enabled = true;
    }

    public void Level1()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }
}
